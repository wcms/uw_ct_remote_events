/**
 * @file
 */

(function ($) {

  Drupal.behaviors.uw_ct_remote_events = {

    attach: function (context, settings) {

        if ($('#edit-field-event-site-und').length > 0) {
            $.getJSON("https://api.uwaterloo.ca/v2/resources/sites.json?key=dem0hash", function (json) {
                $.each(json.data, function (i, site) {
                   $('#edit-field-event-site-und').append($('<option>').text(site.name).attr('value', site.slug));
                });
            });
        }

        $('#edit-field-event-site-und').on('change', function () {
            $('#edit-field-ms-event-details-und').children().remove();
            $('#edit-title').val('');
            var url = $(this).attr('value');
            $.getJSON("https://api.uwaterloo.ca/v2/events/" + url + ".json?key=dem0hash", function (json) {
                if (jQuery.isEmptyObject(json.data)) {
                    var html = '<div class="form-item form-type-radio form-item-field-ms-event-details-und">';
                    html += 'There are no events for this site.';
                    html += '</div>';
                    $('#edit-field-ms-event-details-und').append(html);
                }
                else {
                    $.each(json.data, function (i, event) {
                        var html = '<div class="form-item form-type-radio form-item-field-ms-event-details-und">';
                        html += '<input id="edit-field-ms-event-details-und-"' + event.id + ' class="form-radio" type="radio" value="' + event.id + '" name="field_ms_event_details[und]" data-title="' + event.title + '">';
                        html += '<label class="option" for="edit-field-ms-event-details-und-' + event.id + '"> ' + event.title;
                        if (event.times) {
                            $.each(event.times, function (j, times) {
                               html += '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + times.start.substring(0, 10);
                            });
                        }
                        html += '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="' + event.link + '">' + event.link + '</a>';
                        html += '</div>';
                        $('#edit-field-ms-event-details-und').append(html);
                    });
                }
             });
        });

        $('#edit-field-ms-event-details-und').on('change', function () {
            $('#edit-title').val($('input:checked', '#edit-field-ms-event-details-und').attr('data-title'));
        });
    }

  };
})(jQuery);
