<?php

/**
 * @file
 * uw_ct_remote_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_remote_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_remote_events_node_info() {
  $items = array(
    'uw_ct_remote_events' => array(
      'name' => t('Remote events'),
      'base' => 'node_content',
      'description' => t('Adding events from other websites.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
